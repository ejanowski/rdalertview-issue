//
//  UIAlertViewController+Orientation.h
//  babynightlight
//
//  Created by Emeric Janowski on 22/04/2015.
//  Copyright (c) 2015 ReservoirDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertController (Orientation)

@end
