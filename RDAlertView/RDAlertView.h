//
//  RDAlertView.h
//  RDAlertViewSample
//
//  Created by Emeric Janowski on 17/04/2015.
//  Copyright (c) 2015 Emeric Janowski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void (^AlertCompletion)(NSInteger buttonIndex, NSInteger cancelButtonIndex, NSArray* textfields);

typedef enum  {
    RDAlertViewStyleDefault = 0,
    RDAlertViewStyleSecureTextInput,
    RDAlertViewStylePlainTextInput,
    RDAlertViewStyleLoginAndPasswordInput
} RDAlertViewStyle;

@interface RDAlertView : NSObject <UIAlertViewDelegate, UINavigationControllerDelegate>
{
    
}

@property (nonatomic, strong) id mAlert;
@property (nonatomic, copy) AlertCompletion mCompletion;
@property (nonatomic) RDAlertViewStyle alertViewStyle;
@property (nonatomic, strong) NSMutableArray* textfields;


+ (RDAlertView*)sharedInstance;
- (void) addNavigationController:(UINavigationController *)_NavController;
- (void) setDeviceOrientation:(NSArray*) deviceOrientations;
- (void) setInterfaceOrientation:(NSArray*) interfaceOrientations;
- (NSArray*) getSupportedInterfaceOrientation;
- (NSArray*) getSupportedDeviceOrientation;

- (id)initWithTitle:(NSString*) title
            message:(NSString *) message
         completion:(AlertCompletion)completion
  cancelButtonTitle:(NSString*) cancelButtonTitle
  otherButtonTitles:(NSString*) otherButtonTitles,... NS_REQUIRES_NIL_TERMINATION;

- (void) show;

@end
