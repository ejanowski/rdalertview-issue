//
//  RDAlertView.m
//  RDAlertViewSample
//
//  Created by Emeric Janowski on 17/04/2015.
//  Copyright (c) 2015 Emeric Janowski. All rights reserved.
//

#import "RDAlertView.h"
#import "UIAlertViewController+Orientation.h"

#define kCancelButtonIndex 0
#define kAppearStateNormal 34
#define kTransitionDuration 0.3

@implementation RDAlertView


@synthesize alertViewStyle = _alertViewStyle;

#pragma mark -
#pragma mark Singleton Methods

static RDAlertView *sharedInstance = nil;
static bool isNavControllerAnimating;

static NSArray* mSupportedInterfaceOrientation = nil;
static NSArray* mSupportedDeviceOrientation = nil;


+ (RDAlertView*)sharedInstance
{
    if (sharedInstance == nil) {
        sharedInstance = [[super allocWithZone:NULL] init];
        mSupportedDeviceOrientation = [NSArray arrayWithObjects:@(UIDeviceOrientationPortrait), @(UIDeviceOrientationPortraitUpsideDown), nil];
        mSupportedInterfaceOrientation = [NSArray arrayWithObjects:@(UIInterfaceOrientationMaskPortrait), @(UIInterfaceOrientationMaskPortraitUpsideDown), nil];
    }
    
    return sharedInstance;
}



- (void) addNavigationController:(UINavigationController *)_NavController
{
    _NavController.delegate = self;
}

- (void) setDeviceOrientation:(NSArray*) deviceOrientations
{
    mSupportedDeviceOrientation = [NSArray arrayWithArray:deviceOrientations];
}

- (void) setInterfaceOrientation:(NSArray*) interfaceOrientations
{
    mSupportedInterfaceOrientation = [NSArray arrayWithArray:interfaceOrientations];
}

- (NSArray*) getSupportedInterfaceOrientation
{
    return mSupportedInterfaceOrientation;
}

- (NSArray*) getSupportedDeviceOrientation
{
    return mSupportedDeviceOrientation;
}

- (void) setAnimationToYES
{
    isNavControllerAnimating = NO;
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    NSLog(@"didShowViewController");
    isNavControllerAnimating = NO;
//    [self performSelector:@selector(setAnimationToYES) withObject:nil afterDelay:kTransitionDuration];
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if(animated)
    {
        isNavControllerAnimating = YES;
    }
    else
    {
        isNavControllerAnimating = NO;
    }
    NSLog(@"willShowViewController %d",animated);
}


#pragma mark -
#pragma mark Normal Methods

- (id)initWithTitle:(NSString*) title
            message:(NSString *) message
         completion:(AlertCompletion)completion
  cancelButtonTitle:(NSString*) cancelButtonTitle
  otherButtonTitles:(NSString*) otherButtonTitles,... NS_REQUIRES_NIL_TERMINATION
{
    self = [super init];
    if(self)
    {
        self.mCompletion = completion;
        self.textfields = nil;
        if(NSClassFromString(@"UIAlertController") != nil)
        {
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                           message:message
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:cancelButtonTitle
                                                                   style:UIAlertActionStyleCancel
                                                                 handler:^(UIAlertAction *action) {
                                                                     if(_mCompletion)
                                                                     {
                                                                         self.mCompletion(kCancelButtonIndex,kCancelButtonIndex,_textfields);
                                                                     }
                                                                 }];
            [alert addAction:cancelAction];
            
            
            NSString* eachObject;
            va_list argumentList;
            if (otherButtonTitles)
            {
                int index = 1;
                UIAlertAction* button1 = [UIAlertAction actionWithTitle:otherButtonTitles
                                                                       style:UIAlertActionStyleDefault
                                                                     handler:^(UIAlertAction *action) {
                                                                         if(_mCompletion)
                                                                         {
                                                                             self.mCompletion(index,kCancelButtonIndex,_textfields);
                                                                         }
                                                                     }];
                [alert addAction:button1];
                va_start(argumentList, otherButtonTitles);
                while ((eachObject = va_arg(argumentList, id)))
                {
                    index++;
                    UIAlertAction* buttonX = [UIAlertAction actionWithTitle:eachObject
                                                                      style:UIAlertActionStyleDefault
                                                                    handler:^(UIAlertAction *action) {
                                                                        if(_mCompletion)
                                                                        {
                                                                            self.mCompletion(index,kCancelButtonIndex,_textfields);
                                                                        }
                                                                    }];
                    [alert addAction:buttonX];
                }
                va_end(argumentList);
            }
            self.mAlert = alert;
        }
        else
        {
            UIAlertView* alert = [[UIAlertView alloc] init];
            alert.message = message;
            alert.title = title;
            
            [alert addButtonWithTitle:cancelButtonTitle];
            
            NSString* eachObject;
            va_list argumentList;
            if (otherButtonTitles)
            {
                [alert addButtonWithTitle:otherButtonTitles];
                va_start(argumentList, otherButtonTitles);
                while ((eachObject = va_arg(argumentList, id)))
                {
                    [alert addButtonWithTitle:eachObject];
                }
                va_end(argumentList);
            }
            
            [alert setCancelButtonIndex:0];
            alert.delegate = self;
            self.mAlert = alert;
            [self performSelector:@selector(safetyARC) withObject:nil afterDelay:10];
        }
    }
    return self;
}

- (BOOL) canPresentController:(UIViewController*)_Controller
{
    BOOL canPresent = NO;
    if(_Controller.navigationController != nil)
    {
        canPresent = !isNavControllerAnimating;
    }
    else
    {
        if([_Controller.view.layer.animationKeys count] == 0)
        {
            canPresent = YES;
        }
    }
    return canPresent;
}

- (void) show
{
    if(NSClassFromString(@"UIAlertController") != nil)
    {
        UIAlertController* alert = (UIAlertController*)self.mAlert;
        UIViewController* controller = [self getRootViewController];
//        if((int)[controller valueForKey:@"appearState"] != kAppearStateNormal)
        if(![self canPresentController:controller])
        {
            [self performSelector:@selector(show) withObject:nil afterDelay:0.1];
        }
        else
        {
            isNavControllerAnimating = YES;
            [controller presentViewController:alert
                                     animated:YES
                                   completion:^{
                                       isNavControllerAnimating = NO;
                                   }];
        }
    }
    else
    {
        [((UIAlertView*)self.mAlert) show];
    }
}


- (void)safetyARC
{
    [self performSelector:@selector(safetyARC) withObject:nil afterDelay:10];
}

- (id)getRootViewController {
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    if (window.windowLevel != UIWindowLevelNormal) {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(window in windows) {
            if (window.windowLevel == UIWindowLevelNormal) {
                break;
            }
        }
    }
    return [self topMostViewController:window.rootViewController];
}

- (UIViewController *) topMostViewController: (UIViewController *) controller {
    BOOL isPresenting = NO;
    UIViewController* lController = controller;
    if([controller isKindOfClass:[UINavigationController class]])
    {
        lController = [[(UINavigationController*)controller viewControllers] lastObject];
    }
    do {
        UIViewController *presented = [lController presentedViewController];
        isPresenting = presented != nil;
        if(presented != nil) {
            lController = presented;
        }
        
    } while (isPresenting);
    return lController;
}

- (void)setAlertViewStyle:(RDAlertViewStyle)alertViewStyle
{
    if(NSClassFromString(@"UIAlertController") != nil)
    {
        UIAlertController* alert = (UIAlertController*)self.mAlert;
        switch (alertViewStyle) {
            case RDAlertViewStyleDefault:
                
                break;
            case RDAlertViewStyleSecureTextInput:
            {
                self.textfields = [NSMutableArray array];
                [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
                    [textField setSecureTextEntry:YES];
                    [self.textfields addObject:textField];
                }];
            }
                break;
            case RDAlertViewStylePlainTextInput:
            {
                self.textfields = [NSMutableArray array];
                [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
                    [textField setSecureTextEntry:NO];
                    [self.textfields addObject:textField];
                }];
            }
                break;
            case RDAlertViewStyleLoginAndPasswordInput:
            {
                self.textfields = [NSMutableArray array];
                [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
                    [textField setSecureTextEntry:NO];
                    textField.placeholder = @"Login";
                    [self.textfields addObject:textField];
                }];
                [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
                    [textField setSecureTextEntry:YES];
                    textField.placeholder = @"Password";
                    [self.textfields addObject:textField];
                }];
            }
                break;
            default:
                break;
        }
    }
    else
    {
        UIAlertView* alert = (UIAlertView*)self.mAlert;
        switch (alertViewStyle) {
            case RDAlertViewStyleDefault:
                
                break;
            case RDAlertViewStyleSecureTextInput:
                alert.alertViewStyle = UIAlertViewStyleSecureTextInput;
                self.textfields = [NSMutableArray array];
                [self.textfields addObject:[alert textFieldAtIndex:0]];
                break;
            case RDAlertViewStylePlainTextInput:
                alert.alertViewStyle = UIAlertViewStylePlainTextInput;
                self.textfields = [NSMutableArray array];
                [self.textfields addObject:[alert textFieldAtIndex:0]];
                break;
            case RDAlertViewStyleLoginAndPasswordInput:
                alert.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
                self.textfields = [NSMutableArray array];
                [self.textfields addObject:[alert textFieldAtIndex:0]];
                [self.textfields addObject:[alert textFieldAtIndex:1]];
                break;
            default:
                break;
        }

    }
}

- (void)dealloc
{
    NSLog(@"dealloc");
}

#pragma mark -
#pragma mark UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (alertView.alertViewStyle) {
        case UIAlertViewStyleDefault:
            break;
        case UIAlertViewStyleSecureTextInput:
            self.textfields = [NSMutableArray array];
            [self.textfields addObject:[alertView textFieldAtIndex:0]];
            break;
        case UIAlertViewStylePlainTextInput:
            self.textfields = [NSMutableArray array];
            [self.textfields addObject:[alertView textFieldAtIndex:0]];
            break;
        case UIAlertViewStyleLoginAndPasswordInput:
            self.textfields = [NSMutableArray array];
            [self.textfields addObject:[alertView textFieldAtIndex:0]];
            [self.textfields addObject:[alertView textFieldAtIndex:1]];
            break;
        default:
            break;
    }
    if(_mCompletion)
    {
        self.mCompletion(buttonIndex, alertView.cancelButtonIndex, _textfields);
    }

    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

@end
