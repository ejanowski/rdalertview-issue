//
//  UIAlertViewController+Orientation.m
//  babynightlight
//
//  Created by Emeric Janowski on 22/04/2015.
//  Copyright (c) 2015 ReservoirDev. All rights reserved.
//

#import "UIAlertViewController+Orientation.h"
#import "RDAlertView.h"

@implementation UIAlertController(Orientation)

#pragma mark self rotate
- (BOOL)shouldAutorotate {
    
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    
    BOOL rotate = NO;
    for(NSNumber* number in [[RDAlertView sharedInstance] getSupportedDeviceOrientation])
    {
        if(orientation == [number integerValue])
        {
            rotate = YES;
        }
    }
    
//    if ( orientation == UIDeviceOrientationPortrait
//        | orientation == UIDeviceOrientationPortraitUpsideDown) {
//        
//        return YES;
//    }
    
//    return NO;
    return rotate;
}

- (NSUInteger)supportedInterfaceOrientations {
    NSInteger mask = 0;
    for(NSNumber* number in [[RDAlertView sharedInstance] getSupportedInterfaceOrientation])
    {
        mask = mask | [number integerValue];
    }
    return mask;
//    return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    UIDevice* device = [UIDevice currentDevice];
//    if (device.orientation == UIInterfaceOrientationPortraitUpsideDown) {
//        return UIInterfaceOrientationPortraitUpsideDown;
//    }
    for(NSNumber* number in [[RDAlertView sharedInstance] getSupportedInterfaceOrientation])
    {
        if(device.orientation == [number integerValue])
        {
            return [number integerValue];
        }
    }
    return UIInterfaceOrientationPortrait;
}

@end
