//
//  ViewController.swift
//  RDAlertView-Issue
//
//  Created by Emeric Janowski on 11/05/2015.
//  Copyright (c) 2015 Emeric Janowski. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func showAlert(sender: AnyObject) {
        
        /* Impletation in Objective-C


        RDAlertView* alert = [[RDAlertView alloc] initWithTitle:@"Notification"
                                                        message:message
                                                     completion:^(NSInteger buttonIndex, NSInteger cancelButtonIndex, NSArray *textflieds) { }
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
*/
        
        
        var alert = RDAlertView(title:"Notification",
            message:"message",
            completion: {
                (buttonIndex: NSInteger, cancelButtonIndex:NSInteger, textflieds:NSArray) in
            },
            cancelButtonTitle:"OK",
            otherButtonTitles: nil)
        
        alert.show()
    }
}

